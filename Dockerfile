FROM maven:3.6-openjdk-11
WORKDIR /tmp
COPY . /tmp
RUN mvn clean compile

FROM openjdk:11-jre-slim
COPY --from=0 /tmp/target/*.jar app.jar
CMD ["java", "-jar", "/app.jar"]