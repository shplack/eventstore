package ooo.shplack.eventstore.core;

import ooo.shplack.api.eventstore.Event;
import ooo.shplack.api.eventstore.GetEventsRequest;
import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class ArrayListEventStoreRepositoryTest implements EventStoreRepositoryTest {

    private EventStoreRepository repository;

    @Override
    @Test
    public void It_should_contain_no_events_when_initialized() {
        repository = new ArrayListEventStoreRepository();
        assertEquals(0, repository.getEventsCount());
    }

    @Override
    @Test
    public void It_should_contain_1_event_after_adding_1() throws CreateEventException {
        repository = new ArrayListEventStoreRepository();

        repository.createEvent(newRandomEvent());

        assertEquals(1, repository.getEventsCount());

    }

    @Override
    @Test
    public void It_should_contain_2_event_after_adding_2() throws CreateEventException {
        repository = new ArrayListEventStoreRepository();

        repository.createEvent(newRandomEvent());
        repository.createEvent(newRandomEvent());

        assertEquals(2, repository.getEventsCount());

    }

    @Override
    @Test
    public void It_should_return_1_event_when_aggregate_id_match() throws CreateEventException {
        repository = new ArrayListEventStoreRepository();

        String anAggregateId = null;
        // Create 10 events
        for (int i = 0; i < 10; i++) {
            Event event = newRandomEvent();
            repository.createEvent(event);

            anAggregateId = event.getAggregateId();
        }

        assertEquals(1,
                repository.getEvents(
                        GetEventsRequest.newBuilder().setAggregateId(anAggregateId).build()
                ).size()
        );
    }

    @Override
    @Test
    public void It_should_return_1_event_when_event_type_match() throws CreateEventException {
        repository = new ArrayListEventStoreRepository();

        String anEventType = null;
        // Create 10 events
        for (int i = 0; i < 10; i++) {
            Event event = newRandomEvent();
            repository.createEvent(event);

            anEventType = event.getEventType();
        }

        assertEquals(1,
                repository.getEvents(
                        GetEventsRequest.newBuilder().setEventType(anEventType).build()
                ).size()
        );

    }

    @Override
    @Test
    public void It_should_return_1_event_when_event_type_and_aggregate_id_match() throws CreateEventException {
        repository = new ArrayListEventStoreRepository();

        String anEventType = null;
        String anAggregateId = null;
        // Create 10 events
        for (int i = 0; i < 10; i++) {
            Event event = newRandomEvent();
            repository.createEvent(event);

            anEventType = event.getEventType();
            anAggregateId = event.getAggregateId();
        }

        assertEquals(1,
                repository.getEvents(
                        GetEventsRequest.newBuilder().setEventType(anEventType).setAggregateId(anAggregateId).build()
                ).size()
        );
    }

    @Override
    @Test
    public void It_should_return_no_event_when_no_match() throws CreateEventException {
        repository = new ArrayListEventStoreRepository();

        String anEventType = UUID.randomUUID().toString();
        String anAggregateId = UUID.randomUUID().toString();

        // Create 10 events
        for (int i = 0; i < 10; i++) {
            Event event = newRandomEvent();
            repository.createEvent(event);
        }

        assertEquals(0,
                repository.getEvents(
                        GetEventsRequest.newBuilder().setEventType(anEventType).build()
                ).size()
        );

        assertEquals(0,
                repository.getEvents(
                        GetEventsRequest.newBuilder().setAggregateId(anAggregateId).build()
                ).size()
        );

        assertEquals(0,
                repository.getEvents(
                        GetEventsRequest.newBuilder().setEventType(anEventType).setAggregateId(anAggregateId).build()
                ).size()
        );
    }

    @Override
    public void It_should_fail_when_event_id_already_exists() throws CreateEventException {
        repository = new ArrayListEventStoreRepository();
        Event event = newRandomEvent();

        repository.createEvent(event);
        try {
            repository.createEvent(event);
        } catch (CreateEventException e) {
            return;
        }
        Assert.fail();


    }

    private Event newRandomEvent() {
        String event_id = UUID.randomUUID().toString();
        String event_type = UUID.randomUUID().toString();
        String aggregate_id = UUID.randomUUID().toString();
        String aggregate_type = UUID.randomUUID().toString();
        long aggregate_version = 1L;
        String event_data = "";

        return Event.newBuilder()
                .setEventId(event_id)
                .setEventType(event_type)
                .setAggregateId(aggregate_id)
                .setAggregateType(aggregate_type)
                .setAggregateVersion(aggregate_version)
                .setEventData(event_data)
                .build();
    }
}
