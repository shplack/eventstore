package ooo.shplack.eventstore.core;

public interface EventStoreRepositoryTest {

    // CreateEvent tests
    void It_should_contain_no_events_when_initialized();

    void It_should_contain_1_event_after_adding_1() throws CreateEventException;

    void It_should_contain_2_event_after_adding_2() throws CreateEventException;

    // GetEvents tests
    void It_should_return_1_event_when_aggregate_id_match() throws CreateEventException;

    void It_should_return_1_event_when_event_type_match() throws CreateEventException;

    void It_should_return_1_event_when_event_type_and_aggregate_id_match() throws CreateEventException;

    void It_should_return_no_event_when_no_match() throws CreateEventException;

    // Unique Event_id

    void It_should_fail_when_event_id_already_exists() throws CreateEventException;

}
