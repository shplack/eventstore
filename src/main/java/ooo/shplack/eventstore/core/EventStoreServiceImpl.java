package ooo.shplack.eventstore.core;


import com.google.common.base.Strings;
import io.grpc.stub.StreamObserver;
import ooo.shplack.api.eventstore.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;


public class EventStoreServiceImpl extends EventStoreServiceGrpc.EventStoreServiceImplBase {

    final Logger logger = LoggerFactory.getLogger(EventStoreServiceImpl.class);

    private final EventStoreRepository repository;

    private final NatsPublisher nats;

    public EventStoreServiceImpl(EventStoreRepository repository, NatsPublisher nats) {

        this.repository = repository;
        this.nats = nats;
    }

    @Override
    public void getEvents(final GetEventsRequest request, StreamObserver<GetEventsResponse> responseObserver) {
        Collection<Event> events = repository.getEvents(request);
        logger.debug("Retrieved {} events.{}{}",
                events.size(),
                Strings.isNullOrEmpty(request.getEventType()) ? "" : "\n\tEvent type: " + request.getEventType(),
                Strings.isNullOrEmpty(request.getAggregateId()) ? "" : "\n\tAggregate ID: " + request.getAggregateId());

        responseObserver.onNext(
                GetEventsResponse.newBuilder().addAllEvents(
                        events).build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void createEvent(CreateEventRequest request, StreamObserver<CreateEventResponse> responseObserver) {
        try {
            repository.createEvent(request.getEvent());
            logger.debug("Created {} {}", request.getEvent().getEventType(), request.getEvent().getEventId());

            nats.publishEvent(request.getEvent());

            responseObserver.onNext(
                    CreateEventResponse.newBuilder().setIsSuccess(true).clearError().build()
            );
        } catch (CreateEventException e) {
            logger.error("Failed for event ID {}", request.getEvent().getEventId());
            responseObserver.onNext(
                    CreateEventResponse.newBuilder().setIsSuccess(false).setError("Non unique event ID.").build()
            );
        }
        responseObserver.onCompleted();
    }
}
