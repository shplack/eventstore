package ooo.shplack.eventstore.core;

import ooo.shplack.api.eventstore.Event;
import ooo.shplack.api.eventstore.GetEventsRequest;

import java.util.Collection;

public interface EventStoreRepository {

    Collection<Event> getEvents(GetEventsRequest request);

    int getEventsCount();

    void createEvent(Event request) throws CreateEventException;
}
