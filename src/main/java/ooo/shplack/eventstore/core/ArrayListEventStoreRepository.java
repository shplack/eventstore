package ooo.shplack.eventstore.core;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import ooo.shplack.api.eventstore.Event;
import ooo.shplack.api.eventstore.GetEventsRequest;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ArrayListEventStoreRepository implements EventStoreRepository {

    private List<Event> events = Lists.newArrayList();

    @Override
    public Collection<Event> getEvents(GetEventsRequest request) {

        // If the request is null, no filtering is applied
        if (Objects.isNull(request)) {
            return Collections.unmodifiableList(events);
        }

        return events.stream().filter(event -> eventMatchesRequestFilter(event, request)
        ).collect(Collectors.toUnmodifiableList());
    }

    private boolean eventMatchesRequestFilter(Event event, GetEventsRequest request) {
        return (Strings.isNullOrEmpty(request.getEventType()) || request.getEventType().equals(event.getEventType()))
                && (Strings.isNullOrEmpty(request.getAggregateId()) || request.getAggregateId().equals(event.getAggregateId()));
    }

    @Override
    public int getEventsCount() {
        return events.size();
    }

    @Override
    public void createEvent(Event event) throws CreateEventException {
        if (events.stream().map(Event::getEventId).anyMatch(event.getEventId()::equals)) {
            throw new CreateEventException();
        }
        events.add(event);
    }
}
