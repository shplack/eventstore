package ooo.shplack.eventstore.core;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class App {

    private static final int SERVER_PORT = 8080;

    public static void main(String[] args) throws IOException, InterruptedException {

        final Logger logger = LoggerFactory.getLogger(App.class);

        EventStoreRepository repository = new ArrayListEventStoreRepository();
        NatsPublisher natsPublisher = new NatsPublisherImpl();

        Server server = ServerBuilder.forPort(SERVER_PORT).addService(new EventStoreServiceImpl(repository, natsPublisher)).build();
        server.start();
        logger.info("Server started on port {}", SERVER_PORT);
        server.awaitTermination();


    }
}
