package ooo.shplack.eventstore.core;

import ooo.shplack.api.eventstore.Event;

public interface NatsPublisher {
    void publishEvent(Event event);
}
