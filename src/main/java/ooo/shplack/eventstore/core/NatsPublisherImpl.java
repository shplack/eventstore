package ooo.shplack.eventstore.core;

import io.nats.client.*;
import ooo.shplack.api.eventstore.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class NatsPublisherImpl implements NatsPublisher {

    private static final String NATS_HOST = "nats";
    private static final String NATS_PORT = "4222";

    final Logger logger = LoggerFactory.getLogger(NatsPublisherImpl.class);

    private Connection nc;

    public NatsPublisherImpl() throws IOException, InterruptedException {
        Options options = new Options.Builder().server("nats://" + NATS_HOST + ":" + NATS_PORT).errorListener(new NatsErrorHandler()).build();
        nc = Nats.connect(options);
    }

    @Override
    public void publishEvent(Event event) {
        nc.publish(event.getEventType(), event.toByteArray());
        logger.debug("Published {} {}", event.getEventType(), event.getEventId());
    }

    private static final class NatsErrorHandler implements ErrorListener {

        final Logger logger = LoggerFactory.getLogger(NatsPublisherImpl.class);

        @Override
        public void errorOccurred(Connection connection, String s) {
            logger.error(s);
        }

        @Override
        public void exceptionOccurred(Connection connection, Exception e) {
            logger.error(e.getMessage(), e);
        }

        @Override
        public void slowConsumerDetected(Connection connection, Consumer consumer) {
            logger.warn("Consumer seems to be slow: {} messages pending.", consumer.getPendingByteCount());
        }
    }
}
